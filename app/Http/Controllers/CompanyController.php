<?php

namespace App\Http\Controllers;

use App\Company;
use App\Location;
use Illuminate\Http\Request;

class CompanyController extends Controller
{

   public function index(){

        $companies = Company::all();
        return view('companies.index', compact('companies'));        

   }

   public function create(){

        $locations = Location::all();
        return view('companies.create', compact('locations'));

   }

   public function store(request $req){

         $company = new Company();
         $company->location_id = $req->location;
         $company->name = $req->name;
         $company->status = $req->status;
         $company->save();

         return redirect()->route('companies.index');

    }   

    public function edit ($id){

         $company = Company::find($id);
         $locations = Location::all();
         return view('companies.edit', compact('company','locations'));


   }   

   public function update (request $req, $id){

    $company = Company::find($id);
    $company->location_id = $req->location;
    $company->name = $req->name;
    $company->status = $req->status;
    $company->update();

    return redirect()->route('companies.index');


   }   

   public function delete(request $req, $id){
    $company = Company::find($id);
    $company->delete();

    return redirect()->route('companies.index');

   }

}
