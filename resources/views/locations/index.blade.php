<h1>Sample Crud - Index</h1>

<a href="{{ route('locations.create')}}">ADD</a>


<table border="2">
  <tr>
    <th>Id</th>
    <th>Name</th>
    <th>Locations</th>
    <th>Address</th>
    <th>Status</th>
    <th>Action</th>
  </tr>

   
     @foreach($locations as $location)
 
    <tr>
      <td>{{ $location->id }}</td>
      <td>{{ $location->name }}</td>

      <td>


      @foreach($location->companies as $company)
      {{ $company->name }}

      @endforeach
      </td>
      <td>{{ $location->address }}</td>

      <td>{{ $location->status == 1 ? 'Available' : 'Not Vailable' }}</td>
      <td>
        <a href="{{ route('locations.edit',['id'=>$location->id])}}">Edit</a> 
        <form id="frm" method="post" action="{{ route('locations.delete', ['id' => $location->id]) }}">
        {{ csrf_field() }}
        </form>
        <a href="#" onclick="document.getElementById('frm').submit();">Delete</a>
      </td> 
    </tr>
    @endforeach


</table>