<!DOCTYPE html>
<html>
<body>

<h2>HTML Forms - Edit</h2>

<form action=" {{ route('companies.update', ['id' => $company->id]) }}" method='POST'>
{{ csrf_field() }}
    <label for="lname">Location:</label><br>
    <select name="location" id="">
      @foreach ($locations as $location)
        <option value=" {{ $location->id }} "> {{ $location->name}} </option>
      @endforeach
    </select><br>

  <label for="name">Name:</label><br>
  <input type="text" value=" {{ $company->name }} "  name="name"><br>
  
  <label for="status">Status:</label><br>
    <select name="status" id="status">
        <option {{ $company->status == 1 ? 'selected' : '' }} value="1">Available</option>
        <option {{ $company->status == 2 ? 'selected' : '' }} value="2">Not Available</option>
    </select>  
  <br><br>

  <input type="submit" value="Submit">

</form> 

 

</body>
</html>
