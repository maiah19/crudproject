<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/employee/index',[
    'uses' => 'EmployeeController@index',
    'as' => 'employees.index',
]);


Route::get('/employee/create',[
    'uses' => 'EmployeeController@create',
    'as' => 'employees.create',
]);

Route::post('/employee/store',[
    'uses' => 'EmployeeController@store',
    'as' => 'employees.store',
]);

Route::get('/employee/edit/{id}',[
    'uses' => 'EmployeeController@edit',
    'as' => 'employees.edit',
]);

Route::post('/employee/update/{id}',[
    'uses' => 'EmployeeController@update',
    'as' => 'employees.update',
]);


Route::get('/employee/delete/{id}',[
    'uses' => 'EmployeeController@delete',
    'as' => 'employees.delete',
]);

// ---------------------Companies Route----------------------------------


Route::get('/companies/index',[
    'uses' => 'CompanyController@index',
    'as' => 'companies.index',
]);

Route::get('/companies/create',[
    'uses' => 'CompanyController@create',
    'as' => 'companies.create',
]);

Route::post('/companies/store',[
    'uses' => 'CompanyController@store',
    'as' => 'companies.store',
]);

Route::get('/companies/edit/{id}',[
    'uses' => 'CompanyController@edit',
    'as' => 'companies.edit',
]);

Route::post('/companies/update/{id}',[
    'uses' => 'CompanyController@update',
    'as' => 'companies.update',
]);

Route::post('/companies/delete/{id}',[
    'uses' => 'CompanyController@delete',
    'as' => 'companies.delete',
]);


//---------------------Locations Route----------------------------------

Route::get('/locations/index',[
    'uses' => 'LocationController@index',
    'as' => 'locations.index',
]);
Route::post('/locations/store',[
    'uses' => 'LocationController@store',
    'as' => 'locations.store',
]);
Route::get('/locations/create',[
    'uses' => 'LocationController@create',
    'as' => 'locations.create',
]);
Route::get('/locations/edit/{id}',[
    'uses' => 'LocationController@edit',
    'as' => 'locations.edit',
]);
Route::post('/locations/update/{id}',[
    'uses' => 'LocationController@update',
    'as' => 'locations.update',
]);
Route::post('/locations/delete/{id}',[
    'uses' => 'LocationController@delete',
    'as' => 'locations.delete',
]);